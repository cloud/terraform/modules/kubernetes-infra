locals {
  all_instances = flatten([
    for group in var.worker_nodes : [
      for i in range(group.count) : {
        name        = "${var.infra_name}-${group.name}-${i + 1}"
        flavor      = group.flavor
        volume_size = group.volume_size
        #        ip_address  = format("%s.%d", regex("[0-9]+\\.[0-9]+\\.[0-9]+", var.internal_network_cidr), 21 + i)
      }
    ]
  ])

  # Filter out any default rules that are listed in remove_rules
  filtered_default_security_group_rules = {
    for key, value in local.default_security_group_rules :
    key => value if !contains(var.remove_rules, key)
  }

  # Combine default and custom rules, allowing custom rules to override defaults
  final_security_group_rules = merge(local.filtered_default_security_group_rules, var.custom_security_group_rules)

  default_security_group_rules = {
    # Allow all internal TCP & UDP
    alltcp = {
      description      = "Allow all internal TCP communication."
      direction        = "ingress"
      ethertype        = "IPv4"
      protocol         = "tcp"
      port_range_min   = 0
      port_range_max   = 0
      remote_ip_prefix = "${var.internal_network_cidr}"
    }
    alludp = {
      description      = "Allow all internal UDP communication."
      direction        = "ingress"
      ethertype        = "IPv4"
      protocol         = "udp"
      port_range_min   = 0
      port_range_max   = 0
      remote_ip_prefix = "${var.internal_network_cidr}"
    }

    # External communication
    # HTTP(S)
    https4 = {
      description      = "Allow external HTTPS communication."
      direction        = "ingress"
      ethertype        = "IPv4"
      protocol         = "tcp"
      port_range_min   = 443
      port_range_max   = 443
      remote_ip_prefix = "0.0.0.0/0"
    }
    http4 = {
      description      = "Allow external HTTP communication."
      direction        = "ingress"
      ethertype        = "IPv4"
      protocol         = "tcp"
      port_range_min   = 80
      port_range_max   = 80
      remote_ip_prefix = "0.0.0.0/0"
    }

    # SSH
    ssh4 = {
      description      = "Allow external SSH communication."
      direction        = "ingress"
      ethertype        = "IPv4"
      protocol         = "tcp"
      port_range_min   = 22
      port_range_max   = 22
      remote_ip_prefix = "0.0.0.0/0"
    }
    # ICMP
    icmp4 = {
      description      = "Allow external ICMP communication."
      direction        = "ingress"
      ethertype        = "IPv4"
      protocol         = "icmp"
      port_range_min   = 0
      port_range_max   = 0
      remote_ip_prefix = "0.0.0.0/0"
    }
    # LB
    lb4 = {
      description      = "Allow communication to Kubernetes cluster communication."
      direction        = "ingress"
      ethertype        = "IPv4"
      protocol         = "tcp"
      port_range_min   = var.lb_kube_api_member_port
      port_range_max   = var.lb_kube_api_member_port
      remote_ip_prefix = "0.0.0.0/0"
    }
  }
}
