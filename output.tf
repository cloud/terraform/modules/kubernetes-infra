output "bastion_external_ip" {
  value = openstack_networking_floatingip_v2.bastion_fip.address
}

output "lb_external_ip" {
  value = var.create_lb_kube_api ? openstack_networking_floatingip_v2.lb_kube_api_fip[0].address : null
}

output "control_instance_ip" {
  value = openstack_compute_instance_v2.control_nodes[*].access_ip_v4
}

output "worker_instance_ip" {
  value = [for nodes in openstack_compute_instance_v2.worker_nodes : nodes.access_ip_v4]
}

output "vip_ip" {
  value = var.kube_vip
}

output "vip_fip" {
  value = var.kube_fip ? openstack_networking_floatingip_v2.vip_fip[0].address : null
}
