resource "openstack_lb_loadbalancer_v2" "lb_kube_api" {
  count              = var.create_lb_kube_api ? 1 : 0
  name               = "${var.infra_name}-kube-api-lb"
  description        = "Loadbalancer for ${var.infra_name} Kubernetes API"
  vip_subnet_id      = openstack_networking_subnet_v2.subnet_default[0].id
  security_group_ids = [openstack_networking_secgroup_v2.secgroup_default.id]
  admin_state_up     = "true"
}

resource "openstack_lb_listener_v2" "lb_kube_api_listener" {
  count           = var.create_lb_kube_api ? 1 : 0
  name            = "${var.infra_name}-kube-api-lb-listener"
  protocol        = "TCP"
  protocol_port   = var.lb_kube_api_listener_port
  loadbalancer_id = openstack_lb_loadbalancer_v2.lb_kube_api[0].id
  default_pool_id = openstack_lb_pool_v2.lb_kube_api_pool[0].id
}

resource "openstack_lb_pool_v2" "lb_kube_api_pool" {
  count           = var.create_lb_kube_api ? 1 : 0
  name            = "${var.infra_name}-kube-api-lb-pool"
  protocol        = "TCP"
  lb_method       = "ROUND_ROBIN"
  loadbalancer_id = openstack_lb_loadbalancer_v2.lb_kube_api[0].id

  persistence {
    type = "SOURCE_IP"
  }
}

resource "openstack_lb_member_v2" "lb_kube_api_member" {
  count         = var.create_lb_kube_api ? var.control_nodes_count : 0
  name          = "${var.infra_name}-kube-api-member-${count.index + 1}"
  pool_id       = openstack_lb_pool_v2.lb_kube_api_pool[0].id
  address       = openstack_networking_port_v2.control_ports[count.index].fixed_ip[0].ip_address
  protocol_port = var.lb_kube_api_member_port
}

resource "openstack_lb_monitor_v2" "lb_kube_api_monitor" {
  count       = var.create_lb_kube_api ? 1 : 0
  pool_id     = openstack_lb_pool_v2.lb_kube_api_pool[0].id
  name        = "${var.infra_name}-kube-api-lb-monitor"
  type        = "TCP"
  delay       = 20
  timeout     = 10
  max_retries = 5
}
