## HOW TO WORK WITH THIS REPO

pre-commit
terraform-docs

<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_local"></a> [local](#requirement\_local) | ~> 2.5.1 |
| <a name="requirement_openstack"></a> [openstack](#requirement\_openstack) | ~> 2.0.0 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_local"></a> [local](#provider\_local) | ~> 2.5.1 |
| <a name="provider_openstack"></a> [openstack](#provider\_openstack) | ~> 2.0.0 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [local_file.k8s_inventory](https://registry.terraform.io/providers/hashicorp/local/latest/docs/resources/file) | resource |
| [local_file.k8s_variable](https://registry.terraform.io/providers/hashicorp/local/latest/docs/resources/file) | resource |
| [openstack_compute_instance_v2.bastion](https://registry.terraform.io/providers/terraform-provider-openstack/openstack/latest/docs/resources/compute_instance_v2) | resource |
| [openstack_compute_instance_v2.control_nodes](https://registry.terraform.io/providers/terraform-provider-openstack/openstack/latest/docs/resources/compute_instance_v2) | resource |
| [openstack_compute_instance_v2.worker_nodes](https://registry.terraform.io/providers/terraform-provider-openstack/openstack/latest/docs/resources/compute_instance_v2) | resource |
| [openstack_compute_keypair_v2.pubkey](https://registry.terraform.io/providers/terraform-provider-openstack/openstack/latest/docs/resources/compute_keypair_v2) | resource |
| [openstack_compute_servergroup_v2.anti_affinity_group](https://registry.terraform.io/providers/terraform-provider-openstack/openstack/latest/docs/resources/compute_servergroup_v2) | resource |
| [openstack_lb_listener_v2.lb_kube_api_listener](https://registry.terraform.io/providers/terraform-provider-openstack/openstack/latest/docs/resources/lb_listener_v2) | resource |
| [openstack_lb_loadbalancer_v2.lb_kube_api](https://registry.terraform.io/providers/terraform-provider-openstack/openstack/latest/docs/resources/lb_loadbalancer_v2) | resource |
| [openstack_lb_member_v2.lb_kube_api_member](https://registry.terraform.io/providers/terraform-provider-openstack/openstack/latest/docs/resources/lb_member_v2) | resource |
| [openstack_lb_monitor_v2.lb_kube_api_monitor](https://registry.terraform.io/providers/terraform-provider-openstack/openstack/latest/docs/resources/lb_monitor_v2) | resource |
| [openstack_lb_pool_v2.lb_kube_api_pool](https://registry.terraform.io/providers/terraform-provider-openstack/openstack/latest/docs/resources/lb_pool_v2) | resource |
| [openstack_networking_floatingip_associate_v2.bastion_fip_associate](https://registry.terraform.io/providers/terraform-provider-openstack/openstack/latest/docs/resources/networking_floatingip_associate_v2) | resource |
| [openstack_networking_floatingip_associate_v2.lb_kube_api_fip_associate](https://registry.terraform.io/providers/terraform-provider-openstack/openstack/latest/docs/resources/networking_floatingip_associate_v2) | resource |
| [openstack_networking_floatingip_associate_v2.res_vip_fip_associate](https://registry.terraform.io/providers/terraform-provider-openstack/openstack/latest/docs/resources/networking_floatingip_associate_v2) | resource |
| [openstack_networking_floatingip_v2.bastion_fip](https://registry.terraform.io/providers/terraform-provider-openstack/openstack/latest/docs/resources/networking_floatingip_v2) | resource |
| [openstack_networking_floatingip_v2.lb_kube_api_fip](https://registry.terraform.io/providers/terraform-provider-openstack/openstack/latest/docs/resources/networking_floatingip_v2) | resource |
| [openstack_networking_floatingip_v2.vip_fip](https://registry.terraform.io/providers/terraform-provider-openstack/openstack/latest/docs/resources/networking_floatingip_v2) | resource |
| [openstack_networking_network_v2.network_default](https://registry.terraform.io/providers/terraform-provider-openstack/openstack/latest/docs/resources/networking_network_v2) | resource |
| [openstack_networking_port_v2.bastion_port](https://registry.terraform.io/providers/terraform-provider-openstack/openstack/latest/docs/resources/networking_port_v2) | resource |
| [openstack_networking_port_v2.control_ports](https://registry.terraform.io/providers/terraform-provider-openstack/openstack/latest/docs/resources/networking_port_v2) | resource |
| [openstack_networking_port_v2.vip_port](https://registry.terraform.io/providers/terraform-provider-openstack/openstack/latest/docs/resources/networking_port_v2) | resource |
| [openstack_networking_port_v2.worker_ports](https://registry.terraform.io/providers/terraform-provider-openstack/openstack/latest/docs/resources/networking_port_v2) | resource |
| [openstack_networking_router_interface_v2.router_available_interface](https://registry.terraform.io/providers/terraform-provider-openstack/openstack/latest/docs/resources/networking_router_interface_v2) | resource |
| [openstack_networking_router_interface_v2.router_default_interface](https://registry.terraform.io/providers/terraform-provider-openstack/openstack/latest/docs/resources/networking_router_interface_v2) | resource |
| [openstack_networking_router_v2.router_default](https://registry.terraform.io/providers/terraform-provider-openstack/openstack/latest/docs/resources/networking_router_v2) | resource |
| [openstack_networking_secgroup_rule_v2.secgroup_rules](https://registry.terraform.io/providers/terraform-provider-openstack/openstack/latest/docs/resources/networking_secgroup_rule_v2) | resource |
| [openstack_networking_secgroup_v2.secgroup_default](https://registry.terraform.io/providers/terraform-provider-openstack/openstack/latest/docs/resources/networking_secgroup_v2) | resource |
| [openstack_networking_subnet_v2.subnet_default](https://registry.terraform.io/providers/terraform-provider-openstack/openstack/latest/docs/resources/networking_subnet_v2) | resource |
| [openstack_images_image_v2.nodes_image](https://registry.terraform.io/providers/terraform-provider-openstack/openstack/latest/docs/data-sources/images_image_v2) | data source |
| [openstack_networking_network_v2.external_network](https://registry.terraform.io/providers/terraform-provider-openstack/openstack/latest/docs/data-sources/networking_network_v2) | data source |
| [openstack_networking_router_v2.router_available](https://registry.terraform.io/providers/terraform-provider-openstack/openstack/latest/docs/data-sources/networking_router_v2) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_bastion_flavor"></a> [bastion\_flavor](#input\_bastion\_flavor) | n/a | `string` | `"standard.small"` | no |
| <a name="input_bastion_image"></a> [bastion\_image](#input\_bastion\_image) | Bastion OS: Image name | `string` | `"ubuntu-jammy-x86_64"` | no |
| <a name="input_bastion_name"></a> [bastion\_name](#input\_bastion\_name) | Name of the bastion VM. Must match [a-zA-Z0-9-]+ regexp. | `string` | `"bastion-server"` | no |
| <a name="input_bastion_ssh_user_name"></a> [bastion\_ssh\_user\_name](#input\_bastion\_ssh\_user\_name) | n/a | `string` | `"ubuntu"` | no |
| <a name="input_control_nodes_count"></a> [control\_nodes\_count](#input\_control\_nodes\_count) | ######################## control nodes settings # ######################## | `number` | `3` | no |
| <a name="input_control_nodes_flavor"></a> [control\_nodes\_flavor](#input\_control\_nodes\_flavor) | n/a | `string` | `"hpc.8core-32ram-ssd-ephem"` | no |
| <a name="input_control_nodes_name"></a> [control\_nodes\_name](#input\_control\_nodes\_name) | Name of the nodes. Must match [a-zA-Z0-9-]+ regexp. | `string` | `"control"` | no |
| <a name="input_control_nodes_volume_size"></a> [control\_nodes\_volume\_size](#input\_control\_nodes\_volume\_size) | The size of the volume to create (in gigabytes) for root filesystem. | `string` | `"30"` | no |
| <a name="input_create_lb_kube_api"></a> [create\_lb\_kube\_api](#input\_create\_lb\_kube\_api) | Create Load Balancer for kube-api | `bool` | `false` | no |
| <a name="input_custom_security_group_rules"></a> [custom\_security\_group\_rules](#input\_custom\_security\_group\_rules) | Custom security group rules to override the defaults | <pre>map(object({<br>    description      = string<br>    direction        = string<br>    ethertype        = string<br>    protocol         = string<br>    port_range_min   = number<br>    port_range_max   = number<br>    remote_ip_prefix = string<br>  }))</pre> | `{}` | no |
| <a name="input_infra_name"></a> [infra\_name](#input\_infra\_name) | Infrastructure (profile) name. Used as a name prefix. Must match [a-zA-Z0-9-]+ regexp. | `string` | `"general-tf-demo"` | no |
| <a name="input_internal_network_cidr"></a> [internal\_network\_cidr](#input\_internal\_network\_cidr) | Internal network address, use CIDR notation | `string` | `"10.0.0.0/24"` | no |
| <a name="input_internal_network_creation_enable"></a> [internal\_network\_creation\_enable](#input\_internal\_network\_creation\_enable) | Create dedicated internal network. true/false ~ create new / reuse existing personal network | `bool` | `true` | no |
| <a name="input_internal_network_name"></a> [internal\_network\_name](#input\_internal\_network\_name) | Internal network name. Either dedicated new network or existing personal network name | `string` | `"<var.infra_name>_network"` | no |
| <a name="input_internal_subnet_creation_enable"></a> [internal\_subnet\_creation\_enable](#input\_internal\_subnet\_creation\_enable) | Create dedicated subnet instance. true/false ~ create new / reuse existing personal subnet | `bool` | `true` | no |
| <a name="input_internal_subnet_name"></a> [internal\_subnet\_name](#input\_internal\_subnet\_name) | Internal network subnet name. Either dedicated new subnet or existing personal subnet name | `string` | `"<var.infra_name>_subnet"` | no |
| <a name="input_kube_fip"></a> [kube\_fip](#input\_kube\_fip) | Allocate floating IP for kubespray | `bool` | `false` | no |
| <a name="input_kube_fip_create_port"></a> [kube\_fip\_create\_port](#input\_kube\_fip\_create\_port) | True if you want floating IP address for kube-vip to access kube-api | `bool` | `false` | no |
| <a name="input_kube_vip"></a> [kube\_vip](#input\_kube\_vip) | Internal IP for kube-vip to access kube-api | `string` | `"10.0.0.5"` | no |
| <a name="input_lb_kube_api_listener_port"></a> [lb\_kube\_api\_listener\_port](#input\_lb\_kube\_api\_listener\_port) | Listener port for kube-aip load balancer | `number` | `6443` | no |
| <a name="input_lb_kube_api_member_port"></a> [lb\_kube\_api\_member\_port](#input\_lb\_kube\_api\_member\_port) | Port number where kube-api listens. | `number` | `6443` | no |
| <a name="input_nodes_image"></a> [nodes\_image](#input\_nodes\_image) | Image used for both control and worker servers | `string` | `"ubuntu-jammy-x86_64"` | no |
| <a name="input_public_external_network"></a> [public\_external\_network](#input\_public\_external\_network) | Cloud public external network pool | `string` | `"public-cesnet-195-113-167-GROUP"` | no |
| <a name="input_remove_rules"></a> [remove\_rules](#input\_remove\_rules) | List of default rule keys to be removed | `list(string)` | `[]` | no |
| <a name="input_router_creation_enable"></a> [router\_creation\_enable](#input\_router\_creation\_enable) | Create dedicated router instance. true/false ~ create new / reuse existing personal router | `bool` | `true` | no |
| <a name="input_ssh_public_key"></a> [ssh\_public\_key](#input\_ssh\_public\_key) | n/a | `string` | `"~/.ssh/id_rsa.pub"` | no |
| <a name="input_ssh_user_name"></a> [ssh\_user\_name](#input\_ssh\_user\_name) | n/a | `string` | `"ubuntu"` | no |
| <a name="input_worker_nodes"></a> [worker\_nodes](#input\_worker\_nodes) | ######################## worker nodes settings # ######################## | <pre>list(object({<br>    name        = string<br>    flavor      = string<br>    count       = number<br>    volume_size = number<br>  }))</pre> | n/a | yes |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_bastion_external_ip"></a> [bastion\_external\_ip](#output\_bastion\_external\_ip) | n/a |
| <a name="output_control_instance_ip"></a> [control\_instance\_ip](#output\_control\_instance\_ip) | n/a |
| <a name="output_lb_external_ip"></a> [lb\_external\_ip](#output\_lb\_external\_ip) | n/a |
| <a name="output_vip_fip"></a> [vip\_fip](#output\_vip\_fip) | n/a |
| <a name="output_vip_ip"></a> [vip\_ip](#output\_vip\_ip) | n/a |
| <a name="output_worker_instance_ip"></a> [worker\_instance\_ip](#output\_worker\_instance\_ip) | n/a |
<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
