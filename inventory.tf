resource "local_file" "k8s_inventory" {
  content = templatefile("${path.module}/ansible_inventory.tmpl",
    {
      bastion_ssh_user_name   = var.bastion_ssh_user_name
      k8s_bastion_ip          = openstack_networking_floatingip_v2.bastion_fip.address
      k8s_bastion_ip_internal = openstack_compute_instance_v2.bastion.access_ip_v4
      k8s_control_name        = openstack_compute_instance_v2.control_nodes.*.name
      k8s_control_ip          = openstack_compute_instance_v2.control_nodes.*.access_ip_v4
      k8s_worker_name         = [for nodes in openstack_compute_instance_v2.worker_nodes : nodes.name]
      k8s_worker_ip           = [for nodes in openstack_compute_instance_v2.worker_nodes : nodes.access_ip_v4]
      lb_kube_api_fip         = try(openstack_networking_floatingip_v2.lb_kube_api_fip[0].address, "")
    }

  )
  filename = "../ansible/ansible_inventory"
}
resource "local_file" "k8s_variable" {
  content = templatefile("${path.module}/openstack_vars.tmpl",
    {
      k8s_floating_network_id = data.openstack_networking_network_v2.external_network.id
      k8s_network_id          = openstack_networking_network_v2.network_default[0].id
      k8s_subnet_id           = openstack_networking_subnet_v2.subnet_default[0].id

    }

  )
  filename = "../ansible/group_vars/all/openstack_vars.yaml"
}
